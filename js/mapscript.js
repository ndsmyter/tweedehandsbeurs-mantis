function initialize() {
	var mapCanvas = document.getElementById('map-canvas');
	var myLatLng = new google.maps.LatLng(51.019552, 3.769984);
	var mapOptions = {
	  center: myLatLng,
	  zoom: 18,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(mapCanvas, mapOptions);
	var marker = new google.maps.Marker({
		position: myLatLng,
		map: map,
		title: 'Gildenhuis'
	});
}
google.maps.event.addDomListener(window, 'load', initialize);
