<?php
	require_once("connection.php");
	
	$page = 2;
	$year = 2016;

	$adminUsers = array(13, 63);
?>
<script type="text/javascript">
	var inAdminMode = false;
	function adminSubscribe(place, time, dataListId) {
		if (!inAdminMode) {
			return;
		}
		var name = $("#admin-input-" + place + "-" + time).val();
		if (name == "") {
			return;
		}
		$(".refreshing").show();
		var id = $("#users" + dataListId + " [value='" + name + "']").data("value");
		$.ajax({
			type: "POST",
			url: "shiftsDB.php",
			data: "admin=true&page=<?php echo $page; ?>&year=<?php echo $year; ?>&moment=" + time + "&place=" + place + "&name=" + name + (id !== undefined ? "&id=" + id : ""),
			success: function(data){
				refresh(true);
			}
		});
	}
	function adminRemove(id) {
		$(".refreshing").show();
		$.ajax({
			type: "POST",
			url: "shiftsDB.php",
			data: "admin=true&remove=true&id=" + id,
			success: function(data){
				refresh(true);
			}
		});
	}
	function register(time, place, remove){
		$(".refreshing").show();
		$.ajax({
			type: "POST",
			url: "shiftsDB.php",
			data: "page=<?php echo $page; ?>&year=<?php echo $year; ?>&moment="+time+"&place="+place+(remove=="true"?"&remove=true":""),
			success: function(data){
				refresh(true);
			}
		});
	}
	var t = null;
	refresh();
	function refresh(force){
		if(t != null)
			clearTimeout(t);
		var shouldSkipUpdate = inAdminMode && (force === undefined || !force);
		if (shouldSkipUpdate){
			return;
		}
		var spinner = $(".refreshing");
		$.ajax({
			type: "GET",
			url: "shifts.php",
			data: {year: "<?php echo $year; ?>", page: "<?php echo $page; ?>"},
			success: function(data){
				if (!shouldSkipUpdate){
					$("#shift_refresh").html(data);
					if (inAdminMode) {
						enableAdminMode(true);
					}
					t = setTimeout("refresh()", 3000);
				}
				spinner.hide();
			},
			error: function(){
				if (!shouldSkipUpdate){
					t = setTimeout("refresh()", 5000);
				}
				spinner.hide();
			}
		});
	}
	function enableAdminMode(enable){
		inAdminMode = enable;
		if (enable) {
			$(".active-admin-mode").show();
			$(".inactive-admin-mode").hide();
		} else {
			$(".active-admin-mode").hide();
			$(".inactive-admin-mode").show();
			refresh();
		}
	}
</script>
<h1>Shiften tweedehandsbeurs</h1>
<div id="content">
<h3>Inleiding</h3>
<p>
	Vul hieronder in welke shift(s) je graag zou doen.
	Indien je ingeschreven bent voor een shift, maar je zou toch liever iets anders doen kan je altijd nog uitschrijven.
</p>
<p>
	Op de knop staat telkens hoeveel mensen er al ingeschreven zijn voor die shift, alsook het maximaal toegelaten aantal.
	Bijvoorbeeld als er &quot;2/5&quot; staat, dan zijn er al 2 mensen van de 5 toegelaten plaatsen ingeschreven.
	Als het maximaal aantal personen voor een bepaalde shift bereikt is, kan je niet meer inschrijven voor die shift.<br/>
	Als er geen maximaal aantal toegelaten mensen bijvermeld staat, dan is het aantal bijgevolg onbeperkt.
</p>
<p>
	Je kan maar 1 shift per tijdslot uitvoeren.
</p>
<p>
	Als je een partner hebt die graag mee zou helpen. Stuur dan een mail naar
	<script>mail2("nicolasdesmyter", "gmail", "com", "?subject=Shift duikfeest partner", "Nicolas De Smyter");</script>
	met het tijdslot, locatie en de naam van je partner.<br/>
	Als u nog meer informatie of hulp nodig hebt, kan u mailen naar <script>mail2("nicolasdesmyter", "gmail", "com", "?subject=Shift duikfeest hulp", "Nicolas De Smyter");</script>.
</p>
<div class="pull-right">
	<a href="shifts.php?print=true&page=<?php echo $page; ?>&year=<?php echo $year; ?>" target="_blank">Afdrukken</a>
</div>
<h3>Planning</h3>
<p>
	9u00: Opzetten van zaal<br/>
	13u30: Deuren openen voor standhouders<br/>
	14u00: Grote publiek mag binnen
</p>
<?php if (in_array($_SESSION['login_id'], $adminUsers)) { ?>
<div class='pull-right'>
	<button id="admin-btn" type='button' class='btn btn-default' onclick='enableAdminMode(!inAdminMode)'>
		Admin modus:
		<span class='active-admin-mode' style='display: none'>Actief</span>
		<span class='inactive-admin-mode'>Niet actief</span>
	</button>
</div>
<?php } ?>
<span class="refreshing alert alert-success" role="alert" style="display:none"><img src="images/loading_spinner.gif" alt="..."> Gegevens worden opgeslagen...</span>
<div id="shift_refresh"></div>
</div>